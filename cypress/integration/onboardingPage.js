import {onboardingPage} from "../../cypress/fixtures/selectors.js";

describe("The Internet Authentication test", function () {

    beforeEach(function(){
        cy.visit('/')
    });

    it("should be able to click on the formAuthenticationBtn", function () {
            cy.get(onboardingPage.formAuthenticationBtn).click()
    
    });
    it("should not be able to login with invalid username and password", function () {  
            cy.get(onboardingPage.formAuthenticationBtn).click()
            cy.get(onboardingPage.usernameField).click()
            cy.get(onboardingPage.usernameField).type("thomas")
            cy.get(onboardingPage.passwordField).click()
            cy.get(onboardingPage.passwordField).type("SecretPassword!")
            cy.get(onboardingPage.loginBtn).click()
            cy.wait(4000)
            cy.get(onboardingPage.errorMsg).contains("Your username is invalid!")

        });
        it("should be able to login with valid username and password", function () { 
            cy.get(onboardingPage.formAuthenticationBtn).click() 
            cy.get(onboardingPage.usernameField).click()
            cy.get(onboardingPage.usernameField).type("tomsmith")
            cy.get(onboardingPage.passwordField).click()
            cy.get(onboardingPage.passwordField).type("SuperSecretPassword!")
            cy.get(onboardingPage.loginBtn).click()
            cy.wait(4000)
            cy.get(onboardingPage.successMsg).contains("You logged into a secure area!")
            
        });

        
        });
        


        






    
